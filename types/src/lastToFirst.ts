function lastToFirst(str: string): string {
  return str.split("").reverse().join("");
}
console.log(lastToFirst("loop"));
console.log(lastToFirst("ab"));
