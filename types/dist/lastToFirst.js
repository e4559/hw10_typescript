"use strict";
function lastToFirst(str) {
    return str.split("").reverse().join("");
}
console.log(lastToFirst("loop"));
console.log(lastToFirst("ab"));
