"use strict";
const users = [
    {
        name: "Bill",
        login: "bill01",
        surname: "Jobs",
        type: "EMPLOYEE",
        address: { officeId: 123, placeId: 1222 },
    },
    {
        name: "Fill",
        login: "fill007",
        surname: "Filler",
        type: "CONTRACTOR",
        contractorCompanyName: "Microsoft",
    },
    {
        name: "Alex",
        login: "alex777",
        type: "EMPLOYEE",
        address: { officeId: 222, placeId: 333 },
    },
    {
        name: "John",
        login: "coolJohn",
        type: "CONTRACTOR",
        contractorCompanyName: "Apple",
    },
];
function foo(users) {
    return users.reduce((acc, currentVal) => {
        if (currentVal.type === "EMPLOYEE") {
            acc.employees.push(currentVal);
        }
        else {
            acc.contractors.push(currentVal);
        }
        return acc;
    }, {
        employees: [],
        contractors: [],
    });
}
console.log(foo(users));
