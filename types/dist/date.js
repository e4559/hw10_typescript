"use strict";
function getDaysToNewYear(num) {
    const [day, month, year] = "31.12.2021".split(".");
    const lastDayInSeconds = new Date(+year, +month, +day);
    if (typeof num == "string") {
        const [argDay, argMonth, argYear] = num.split(".");
        const argDayInSeconds = new Date(+argYear, +argMonth, +argDay);
        return (+lastDayInSeconds - +argDayInSeconds) / 86400000;
    }
    else {
        const numInSeconds = num.getTime();
        return (+lastDayInSeconds - numInSeconds) / 86400000;
    }
}
console.log(getDaysToNewYear("22.04.2020"));
console.log(getDaysToNewYear(new Date(2021, 5, 15)));
