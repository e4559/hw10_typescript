interface Obj {
  serviceName: string;
  serviceId: number;
}
interface NewObj {
  [key: string]: string;
}

function logger(
  arr: string[],
  obj: Obj = { serviceName: "global", serviceId: 1 }
) {
  let newObj: NewObj = {};
  for (let i = 0; i < arr.length; i++) {
    newObj[`${obj.serviceId}-${i}`] = `[${obj.serviceName}] ${arr[i]}`;
  }
  return newObj;
}

console.log(
  logger(["Wrong email", "Wrong password", "Success login"], {
    serviceName: "auth_service",
    serviceId: 3,
  })
);
console.log(logger(["Fatal error", "Data corrupted"]));
