"use strict";
function logger(arr, obj = { serviceName: "global", serviceId: 1 }) {
    let newObj = {};
    for (let i = 0; i < arr.length; i++) {
        newObj[`${obj.serviceId}-${i}`] = `[${obj.serviceName}] ${arr[i]}`;
    }
    return newObj;
}
console.log(logger(["Wrong email", "Wrong password", "Success login"], {
    serviceName: "auth_service",
    serviceId: 3,
}));
console.log(logger(["Fatal error", "Data corrupted"]));
