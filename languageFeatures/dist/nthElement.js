"use strict";
function everyNth(arr, n = 1) {
    let newArr = [];
    for (let i = n; i <= arr.length; i += n) {
        newArr.push(arr[i - 1]);
    }
    return newArr;
}
console.log(everyNth([1, 2, 3, 4, 5, 6], 3));
