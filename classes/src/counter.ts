class Counter {
  count: number;
  constructor() {
    this.count = Counter.x++;
  }
  static x = 0;
  static getInstance() {
    return new Counter();
  }
  static destroy() {
    this.x = 0;
  }
  increase() {
    this.count += 1;
  }
  getState() {
    return this.count;
  }
}

const instance1 = Counter.getInstance();
instance1.increase();
console.log(instance1.getState()); // 1;
const instance2 = Counter.getInstance();
console.log(instance2.getState()); // 1;
instance2.increase();
console.log(instance1.getState()); // 2;   /// sxal ka, instance 2i increaseic instance1y chi poxvum
console.log(instance2.getState()); // 2;
Counter.destroy();
Counter.getInstance().getState(); // 0
