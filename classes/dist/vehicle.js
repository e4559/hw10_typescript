"use strict";
class BaseVehicle {
    constructor(type, tank, startCons, workCons) {
        this.type = type;
        this.tank = tank;
        this.startCons = startCons;
        this.workCons = workCons;
        this.id = 0;
        this.maxFluel = tank; // aranc maxFluel  refuelum error 
    }
    start() {
        this.tank -= this.startCons;
        if (this.tank) {
            this.id = setInterval(() => {
                this.tank -= this.workCons;
            }, 1000);
        }
    }
    stop() {
        clearInterval(this.id);
    }
    refuel() {
        this.tank = this.maxFluel;
    }
}
const car = new BaseVehicle("Car", 40, 3, 1);
car.start();
car.stop();
