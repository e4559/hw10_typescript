"use strict";
class Counter {
    constructor() {
        this.count = Counter.x++;
    }
    static getInstance() {
        return new Counter();
    }
    static destroy() {
        this.x = 0;
    }
    increase() {
        this.count += 1;
    }
    getState() {
        return this.count;
    }
}
Counter.x = 0;
const instance1 = Counter.getInstance();
instance1.increase();
console.log(instance1.getState()); // 1;
const instance2 = Counter.getInstance();
console.log(instance2.getState()); // 1;
instance2.increase();
console.log(instance1.getState()); // 2;
console.log(instance2.getState()); // 2;
Counter.destroy();
Counter.getInstance().getState(); // 0
